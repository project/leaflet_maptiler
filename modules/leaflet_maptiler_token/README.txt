
INSTALLATION
============
This submodule provides a token in order to render a map without the need of using an iFrame.

To install this module you can simply enable it like any other module.

USAGE
============
After a successful installation, the new token will be available and can be used according to the following set of properties (separated by '+' sign) [maptiler:lat_lng_zoom_height:<latitude>+<longitude>+<zoom>+<height>], where:

Latitude: a decimal number that represents the latitude where the marker will be positioned;
Longitude: a decimal number that represents the latitude where the marker will be positioned;
Zoom: an integer that represents the initial value of zoom when viewing the map;
Height: an integer that represents the height (in pixels) of the map.