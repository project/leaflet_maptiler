# Leaflet Maptiler

The purpose of this module is to provide an integration with Maptiler maps using
Leaflet API.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/leaflet_maptiler).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/leaflet_maptiler).


## Requirements

- [Leaflet javascript library](http://leafletjs.com/download.html) - Drop the
unzipped folder in sites/all/libraries and rename it to leaflet, so that the
path to the essential javascript file becomes:
sites/all/libraries/leaflet/leaflet.js


## Installation

Before you enable the Leaflet Maptiler module, you need to download and enable
the Leaflet module and the Libraries module.

If all's ok, you won't see any errors in the Status Report admin/reports/status.

After this all you have to do is enable Leaflet Maptiler to enhance your
mapping experience using maps from Maptiler.


## Configuration

You select the map when you format a single field (eg Geofield) as a
map or when you format a View (of multiple nodes or users) as a map. The module
"IP Geolocation Views and Maps" is particularly good for this.

You can configure Maptiler settings at: `admin/config/leaflet_maptiler`. Here
you should set your Maptiler API Key, layers and the attribution links. If you
set more than 1 layer for your map, a layer switcher will automatically appear
in the upper right-hand corner.
